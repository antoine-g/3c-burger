<?php

namespace Kits\BurgerBundle\Controller;

use Kits\BurgerBundle\Entity\Burger;
use Kits\BurgerBundle\Form\BurgerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(){
        $repository = $this->getDoctrine()->getRepository('KitsBurgerBundle:Burger');
        $burgers = $repository->createQueryBuilder('q')
          ->getQuery()
          ->getArrayResult();
        return new JsonResponse($burgers);
    }

    public function addAction(Request $request){
        $burger = new Burger();

        // == Create form ==
        $form = $this->createForm(BurgerType::class, $burger);

        // == if posted data ==
        if ($request->isMethod('POST'))
        {
            // == if posted data valid ==
            if ($form->handleRequest($request)->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($burger);
                $em->flush();

                $request->getSession()->getFlashBag()->add('info', 'Burger ' . $burger->getName() . ' ajouté.');
            }
            return $this->redirectToRoute('kits_burger_list');
        }

        return $this->render('KitsBurgerBundle:Default:add.html.twig',['form' => $form->createView()]);
    }

    public function editAction($id, Request $request){

        // == Get burger ==
        $burger = $this->getDoctrine()
          ->getManager()
          ->find('KitsBurgerBundle:Burger',$id);

        // == Create form ==
        $form = $this->createForm(BurgerType::class, $burger);

        // == if posted data ==
        if ($request->isMethod('POST'))
        {
            // == if posted data valid ==
            if ($form->handleRequest($request)->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($burger);
                $em->flush();

                $request->getSession()->getFlashBag()->add('info', 'Burger '. $burger->getName() . ' modifié !');
            }
            return $this->redirectToRoute('kits_burger_list');
        }
        return $this->render('KitsBurgerBundle:Default:edit.html.twig',[
          'form' => $form->createView(),
          'burger' => $burger
        ]);
    }

    public function deleteAction($id, Request $request){
        $em = $this->getDoctrine()->getManager();
        $burger = $em->getRepository('KitsBurgerBundle:Burger')->find($id);
        $em->remove($burger);
        $em->flush();

        return $this->redirectToRoute('kits_burger_list');
    }

    public function listAction(){
        $burgers = $this->getDoctrine()->getRepository('KitsBurgerBundle:Burger')->findAll();
        return $this->render('KitsBurgerBundle:Default:list.html.twig',['burgers' => $burgers]);
    }

    public function formAction(){
        return $this->render('KitsBurgerBundle:Default:form.html.twig');
    }

}